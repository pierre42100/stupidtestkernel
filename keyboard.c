#include "asm.h"

int kbd_next_event()
{
	int i;

	do { 
	    i = inb(0x64); 
	} while((i & 0x01) == 0);

	return inb(0x60);
}