#include "vga.h"
#include "lib.h"

int atoi(const char *str)
{
    while (*str == ' ')
        str++;

    int is_negative = 0;
    int val = 0;

    if (*str == '-' || *str == '+')
    {
        is_negative = *str == '-';
        str++;
    }

    while (*str != '\0')
    {
        if (*str < '0' || *str > '9')
            return 0;

        val *= 10;
        val += *str - '0';

        str++;
    }

    return is_negative ? -val : val;
}

char *itoa(int value, char *s)
{
    char *orig_s = s;

    if (value < 0)
    {
        *(s++) = '-';
        value = -value;
    }

    if (value >= 10)
    {
        itoa(value / 10, s);

        while (*s != '\0')
            s++;
    }

    *(s++) = '0' + value % 10;

    *s = '\0';

    return orig_s;
}


int fact(int n)
{
    if (n == 0)
        return 1;

    return n * fact(n - 1);
}

int putchar(int c)
{
    return putchar_color(c, WHITE_COLOR);
}

int puts(const char *str)
{
    return print_line(str, WHITE_COLOR);
}