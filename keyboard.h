/**
 * Get next keyboard event
 */
int kbd_next_event();

static inline int kbd_is_pressed(int code)
{
	return code < 0x80;
}

static inline int kbd_is_released(int code)
{
	return code >= 0x80;
}

static inline int kbd_down_arrow_pressed(int code)
{
	return code == 80;
}

static inline int kbd_up_arrow_pressed(int code)
{
	return code == 72;
}


static inline int kbd_left_arrow_pressed(int code)
{
	return code == 75;
}


static inline int kbd_right_arrow_pressed(int code)
{
	return code == 77;
}


static inline int kbd_enter_pressed(int code)
{
	return code == 28;
}