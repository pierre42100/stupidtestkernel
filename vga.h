#pragma once

#define VGA_ADDRESS 0xB8000   /* video memory begins here. */

/* VGA provides support for 16 colors */
#define BLACK 0
#define GREEN 2
#define RED 4
#define YELLOW 14
#define WHITE_COLOR 15

void init_vga(void);

void clear_screen(void);
int putchar_color(char c, unsigned char color);
int print_line(const char *str, unsigned char color);