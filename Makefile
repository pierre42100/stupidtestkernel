CP := cp
RM := rm -rf
MKDIR := mkdir -pv

BIN = kernel
CFG = grub.cfg
ISO_PATH := iso
BOOT_PATH := $(ISO_PATH)/boot
GRUB_PATH := $(BOOT_PATH)/grub

OBJS = boot.o kernel.o lib.o vga.o keyboard.o

.PHONY: all
all: bootloader kernel linker iso
	@echo Make has completed.

bootloader: boot.asm
	nasm -f elf32 boot.asm -o boot.o

%.o: %.c
	gcc -m32 -fno-stack-protector -o $@ -c $<

kernel: $(OBJS)
	gcc -m32 -fno-stack-protector -c -o $@ $< 

linker: linker.ld $(OBJS)
	ld -m elf_i386 -T linker.ld -o kernel $(OBJS)

iso: kernel
	$(MKDIR) $(GRUB_PATH)
	$(CP) $(BIN) $(BOOT_PATH)
	$(CP) $(CFG) $(GRUB_PATH)
	grub-file --is-x86-multiboot $(BOOT_PATH)/$(BIN)
	grub-mkrescue -o my-kernel.iso $(ISO_PATH)

.PHONY: clean run_os all

clean:
	$(RM) *.o $(BIN) *iso

run_os: all iso
	qemu-system-i386 -cdrom my-kernel.iso