#include "vga.h"

unsigned short *terminal_buffer;
unsigned int vga_index;

void init_vga(void)
{
	/* TODO: Add random f-word here */
    terminal_buffer = (unsigned short *)VGA_ADDRESS;
    vga_index = 0;
}

void clear_screen(void)
{
    int index = 0;
    /* there are 25 lines each of 80 columns;
       each element takes 2 bytes */
    while (index < 80 * 25 * 2) {
            terminal_buffer[index] = ' ';
            index += 1;
    }
}

int putchar_color(char c, unsigned char color)
{
    if (c == '\n')
    {
        vga_index += 80 - vga_index % 80;
    }

    else {
        terminal_buffer[vga_index] = (unsigned short)c|(unsigned short)color << 8;
        vga_index++;
    }

    // TODO : shift screen if bottom is reached
    if (vga_index >= 80*25)
    {
        vga_index = 80*24;

        for (int i = 0; i < 80*24; i++)
            terminal_buffer[i] = terminal_buffer[i + 80];

        for (int i = 0; i < 80; i++)
            terminal_buffer[80*24 + i] = ' ';
    }

    return c;
}

int print_line(const char *str, unsigned char color)
{
    int index = 0;
    while (str[index]) {
        putchar_color(str[index], color);    
        index++;
    }

    putchar_color('\n', color);

    return index;
}