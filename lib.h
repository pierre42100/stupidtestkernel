#pragma once

int fact(int n);
char *itoa(int value, char *s);
int atoi(const char *str);

int putchar(int c);
int puts(const char *str);